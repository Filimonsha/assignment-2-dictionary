;rdi - string start, rsi - dict start
%include lib.inc

%define offset 8

global find_word

section .text
find_word:
	push rdi
	push rsi
	add rsi, offset
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jne .if_found

	mov rsi, [rsi]
	test rsi, rsi
	jne find_word 
	xor rax, rax
	ret

.if_found:
	mov rax, rsi
	ret
