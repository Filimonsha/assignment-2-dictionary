%define pointer 0

%macro colon 2
	%ifid %2
		%2: dq pointer
		%define pointer %2
	%else	
		%error "Don't use restricted symbols"
	%endif
	%ifstr %1
		db %1, 0
	%else
		%error "Insert strings, please"
	%endif
%endmacro
	
