%.o: %.asm
	nasm -felf64 -o $@ $<

build: dict.o lib.o main.o
	ld -o $@ $^

.PHONY: clean
clean:
	rm *.o
	rm build
