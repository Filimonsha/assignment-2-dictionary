%include "lib.inc"
%include "colon.inc"
%include "words.inc"

extern find_word

%define buffer_length 255
%define offset 8

section .rodata

hello_message: db "Insert key to search in dictionary", 0
if_found_message: db "Found value:", 0
divider: db ": ", 0
if_not_found_message: db "No value found", 0
if_overflow: db "Key length can not be more than 255 symbols", 0

section .bss

buffer: times buffer_length db 0

section .text

global _start
_start:
	xor rax, rax
	xor rdi, rdi
	xor rsi, rsi

	mov rdi, hello_message
	call print_string
	call print_newline

	mov rdi, buffer
	mov rsi, buffer_length
	call read_word

	push rdx
	test rax, rax
	je .overflow
	mov rdi, rax
	mov rsi, pointer
	call find_word
	test rax, rax
	je .word_not_found

	push rax
	mov rdi, if_found_message
	call print_string
	call print_newline
	pop rax

	add rax, offset
	mov rdi, rax

	push rdi
	call print_string
	mov rdi, divider
	call print_string
	pop rdi

	pop rdx
	add rdi, rdx
	inc rdi
	call print_string
	call print_newline
	xor rdi, rdi
	call exit

.overflow:
	mov rdi, if_overflow
	jmp .print_error

.word_not_found:
	mov rdi, if_not_found_message

.print_error:
	call print_error
	call print_newline
	mov rdi, 1
	call exit
